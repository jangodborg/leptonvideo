#ifndef TEXTTHREAD
#define TEXTTHREAD

#include <ctime>
#include <stdint.h>

#include <QThread>
#include <QtCore>
#include <QPixmap>
#include <QImage>
#include <QString>
#include <iostream>
#include <QPainter>
#include <cstdlib>


#define PACKET_SIZE 164  //Bytes
#define PACKET_SIZE_UINT16 (PACKET_SIZE/2)   
#define NUMBER_OF_SEGMENTS 4
#define PACKETS_PER_SEGMENT 60
#define PACKETS_PER_FRAME (PACKETS_PER_SEGMENT*NUMBER_OF_SEGMENTS)
//#define FRAME_SIZE_UINT16 (PACKET_SIZE_UINT16*PACKETS_PER_FRAME)
//#define FPS 27;


#define FRAME_W 160
#define FRAME_H 120

#define FRAME_SIZE (FRAME_H*FRAME_W)
#define FRAME_SIZE_UINT16 (PACKET_SIZE_UINT16*PACKETS_PER_FRAME)



class LeptonThread : public QThread
{
  Q_OBJECT;

public:
  LeptonThread();
  ~LeptonThread();

  void run();


public slots:
  void performFFC();
  void snapshot();
  void drawSquare(float);

signals:
  void updateText(QString);
  void updateImage(QImage);

private:

  QImage myImage;

  const char  *m_leftfilename;	// from Left  Sensor
  const char	*m_rightfilename; 	// from Right Sensor

  const char  *m_leftfilenameLocal;		// local from Left  Sensor
  const char	*m_rightfilenameLocal; 	// local from Right Sensor

  uint16_t result[FRAME_H*FRAME_W];
  uint16_t *frameBuffer;


};

#endif
