#include "LeptonThread.h"
#include "Palettes.h"
#include "SPI.h"
#include "Lepton_I2C.h"
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <chrono>

uint8_t mode;
int snapshotCount = 0;
int frame = 0;
static int raw [120][160];

static void pabort(const char *s)
{
	perror(s);
	exit(-7);
}


static void readBinFile( const char* name, uint16_t* Buf, ssize_t size)
{
	int rfd = open(name, O_RDONLY );
	if( rfd == -1) {
		pabort("saveBinFile() ");
	}
	ssize_t readsize = read( rfd, Buf, size);
	if( readsize != size )
	{
		printf("Illegal file size read %d, expected %d \n" ,readsize, size );
		//# pabort("readBinFile() ");
	}
	close(rfd);
}

// check a file exis and has the correct size
static int checkFile( const char* filename, ssize_t filesize )
{
	struct stat statBuf;
	int rc = stat(filename, &statBuf);
	if( rc == 0 && statBuf.st_size == filesize )
		return true;
	else
		return false;
}


static void deleteFile( const char* filename )
{
	int rc = unlink( filename );
	if( rc != 0)
	{
		printf("prob unlinking file %s\n", filename);
		perror("deleteFile()");
	}
}

LeptonThread::LeptonThread() : QThread()
{
//SpiOpenPort(0);
	  m_leftfilename  = "/var/tmp/ir_picture_0.bin" ;		// from Left  Sensor
	  m_rightfilename = "/var/tmp/ir_picture_1.bin" ; 	// from Right Sensor

	  m_leftfilenameLocal  = "ir_picture_0.bin" ;		// from Left  Sensor
	  m_rightfilenameLocal = "ir_picture_1.bin" ; 	// from Right Sensor

}

LeptonThread::~LeptonThread() {
}

void LeptonThread::run()
{
	//create the initial image
	QRgb red = qRgb(255,0,0);
    myImage = QImage(160, 120, QImage::Format_RGB888);

	for(int ix=0;ix<160;ix++) {
	for(int jy=0;jy<120;jy++) {
		myImage.setPixel(ix, jy, red);
		}
	}

	std::cout << "Image initialized " << std::endl;

	emit updateImage(myImage);
	usleep( 1000*5);

	while( true )
	{
		if( checkFile( m_rightfilename, FRAME_SIZE*sizeof(uint16_t)) )
		{
			readBinFile( m_rightfilename, result, sizeof(result));

			deleteFile( m_rightfilename );	// delete after use.
		}
		else
		{
			// std::this_thread::sleep_for(std::chrono::milliseconds(1000));
			usleep( 1000*1000 );

			continue;
		}

		std::cout << "read file: " << m_rightfilename << std::endl;

		frameBuffer = (uint16_t *)result;
		int row, column;
		uint16_t value;
		uint16_t minValue = 65535;
		uint16_t maxValue = 0;
		
		for(int i=0;i<FRAME_SIZE;i++)
		{
			value = frameBuffer[i]/100;  // in degCelcius
		//	value = frameBuffer[i];      // in raw values as delivered ?
			if(value > maxValue)
			{
				maxValue = value;
			}
			if(value < minValue) {
				if(value != 0)
					minValue = value;
			}		
		}
		
//		std::cout << "Minima: " << raw2Celsius(minValue) <<" °C"<< std::endl;
//		std::cout << "Maximo: " << raw2Celsius(maxValue) <<" °C"<< std::endl;

		std::cout << "Minima: " << minValue <<" °C"<< std::endl;
		std::cout << "Maximo: " << maxValue <<" °C"<< std::endl;

		float diff = maxValue - minValue;
		float scale = 255/diff;

		std::cout << "diff: "  << diff  << std::endl;
		std::cout << "scale: " << scale << std::endl;

		QRgb color;
		float valueCenter = 0;
		int k;
	//	for(int k=0; k<FRAME_SIZE; k++)
		for(int jy=0; jy<FRAME_H; jy++) {
		for(int ix=0; ix<FRAME_W; ix++) {
			//myImage.setPixel(ix, jy, red);

			k = ix + jy*FRAME_W ;

			// std::cout << " " << k /*<< std::endl*/ ;

			// value is scaled for color
			value = ((frameBuffer[k]/100) - minValue) * scale;
			//printf("%u\n", frameBuffer[k]);
			
			const int *colormap = colormap_glowBow;

			// map values to color code
			color = qRgb(colormap[3*value], colormap[3*value+1], colormap[3*value+2]);
			
//				// new calc: calc row,col from 'k'
//				if((k/PACKET_SIZE_UINT16) % 2 == 0)
//				{ //1
//					column = (k % PACKET_SIZE_UINT16 - 2);
//					row = (k / PACKET_SIZE_UINT16)/2;
//				}
//				else
//				{//2
//					column = ((k % PACKET_SIZE_UINT16 - 2))+(PACKET_SIZE_UINT16-2);
//					row = (k / PACKET_SIZE_UINT16)/2;
//				}

				column = ix ;
				row	   = jy ;
				//raw[row][column] = frameBuffer[k]/100;

				if(column == (160-1) && row == (120-1))	// last position
					valueCenter = frameBuffer[k];
								
				myImage.setPixel(column, row, color);
		} } // end pixel loop

		std::cout << "end pixel loop. k: " << k << std::endl ;


		drawSquare(valueCenter);
		//lets emit the signal for update
		emit updateImage(myImage);
		frame++;

		///if(frame == 5)
		{
			//# snapshot();
			//abort();
		}

		//# std::cout << "end-while " << std::endl;

	} // end-while
	
	//finally, close SPI port just bcuz
	//SpiClosePort(0);

	std::cout << "end Lepton::run " << std::endl;
}

void LeptonThread::drawSquare(float ){
	QPainter paint(&myImage);
	paint.setPen(Qt::blue);
	//paint.drawRect(79,59,2,2);
	//paint.setPen(Qt::black);
	paint.setFont(QFont("Arial", 8));
	//paint.drawText(84,64, QString("%1 C").arg(raw2Celsius(value)));
}

void LeptonThread::snapshot(){
	snapshotCount++;
	//----------------------- criando imagem ------------------------
	//variables
	struct stat buf;
	const char *inicio = "rgb";
	const char *fim = ".png";
	char meio[32];
	//convert from int to string
	sprintf(meio, "%d", snapshotCount);
	char name[64];
	//appending photo name
	strcpy(name, inicio);
	strcat(name, meio);
	strcat(name, fim);
	//if this name already exists
	int exists = stat(name,&buf);
	//if the name exists stat returns 0
		while(exists == 0){
			//try next number
			snapshotCount++;
			strcpy(name, inicio);
			sprintf(meio, "%d", snapshotCount);
			strcat(name, meio);
			strcat(name, fim);
			exists = stat(name, &buf);
		}
	//saving photo
	myImage.save(QString(name), "PNG", 100);
	
	//---------------------- criando txt -----------------------
	//creating file name
	fim = ".txt";
	strcpy(name, inicio);
	strcat(name, meio);
	strcat(name, fim);
	
	FILE *arq = fopen(name,"wt");
	char valor[64];

	for(int i = 0; i < 120; i++){
			for(int j = 0; j < 160; j++){
		//		sprintf(valor, "%f", raw2Celsius(raw[i][j]));
				sprintf(valor, "%f", float(raw[i][j]));
				fputs(valor, arq);
				fputs(" ", arq);
			}
			fputs("\n", arq);
	}
	fclose(arq);
}


void LeptonThread::performFFC() {
	//perform FFC
	lepton_perform_ffc();
}
